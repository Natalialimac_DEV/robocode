<h3> Método run </h3>

Ao iniciar o robô ele anda 700px, gira para a direita 360 graus e vai girando a arma de 15 em 15 para a direita

<h3> Método onScannedRobot </h3>

Assim que o lime "identificar" um outro robô ele volta 10px e atira com a força do tiro 1

<h3> Método onHitByBullet </h3>

Se o robô lime for atingido ele segue para frente 700px

<h3> Método onHitWall </h3>

Caso ele encontre uma parede, ele volta 20px, logo em seguida repete o método run.

<strong> Analisando as batalhas feitas. O ponto forte do robô lime, por ele sempre está em movimento, ele consegue analisar com o seu radar o robô que está por perto, facilitando quando ele deve atirar. Já o ponto franco é que o movimento em circulo pode deixar ele vulnerável as vezes </strong>

<p>Ter essa experiência criando um robô foi muito divertido. Ficar batalhando várias vezes para descobrir uma forma de vencer a maioria das batalhas com um código não muito complicado, foi muito legal </p>