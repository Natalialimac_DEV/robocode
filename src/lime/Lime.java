package lime;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Lime - a robot by (your name here)
 */
public class Lime extends AdvancedRobot
{
	public void run() {
		
		setColors(Color.pink, Color.black, Color.red); // body,gun,radar
		
		while(true) {
			setAhead(700);
			setTurnRight(360);
			execute();
			setTurnGunRight(15);
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		setBack(10);
		setFire(1);
	}

	public void onHitByBullet(HitByBulletEvent e) {
		setAhead(700);
	}
	
	public void onHitWall(HitWallEvent e) {
		setBack(20);
		
	}	
}
